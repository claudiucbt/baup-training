FROM ubuntu:latest

RUN apt update \
    && apt install -y python3 \
    && apt install -y python3-pip \
    && yes | pip3 install flask \
    && apt install -y curl

COPY final.py /opt

ENTRYPOINT ["python3",  "/opt/final.py"]

EXPOSE 5000
