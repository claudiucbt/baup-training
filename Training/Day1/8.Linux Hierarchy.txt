In LINUX everything is a file!! ca vorbim de fisiere obisnuite, directoare sau fisiere speciale, 
vorbim tot despre un fisier. asta ar trebui sa simplifice mult felul nostru de a vedea linuxul
linux folders:

/ locul de unde plecam, radacina daca vreti, echivalentul lui c: in windows

1. root - home-ul userului root
2. bin - comenzi binare (executabile). este si defaultul in $PATH unde sistemul cauta cand folosim
o comanda. echo $PATH
3. boot - contine bootloaderul si informatii de bootare. e bine sa nu schimbam nimic aici decat daca stim bine ce facem
4. dev - aici se gasesc terminalele (tty). ex: webcam, keyboard, mouse, partitions..
5. etc - "etcetera" - aici gasim fisierele de configurare. EX: aplicatia Libre Office, unde 
fiecare user poate sa isi tina configurarile lui
6. home - directoarele personale ale utilizatorilor
7.a lib - biblioteci utilizate de sistem. bibliotecile astea sunt fisiere de care aplicatiile au
nevoie pentru a putea functiona. acestea sunt cerute in BIN/SBIN de exemplu
7.b lib64 - bibliotecile pe 64 bit
8. mnt,media - aici gasim directoarele unde gasim driverele montate ca: floppy disk, usb, external
HDD... Daca instalam totusi manual un drive, ar fi mai bine sa il instalam in MNT dar pathul default
este MEDIA.
9. opt - folderul optional - daca de exemplu cream un software, ar fi indicat sa fie creat aici first, sau de ce nu, o imprimanta
10. sbin - binarele (executabele) sistemului de operare. pentru a le modifica este nevoie de root dar nu va recomand
11. srv - service directory - aigi gasim stocate date despre un web server de exemplu sau un ftp
aici vei tine datele shared
12. tmp - directoriul temporar - aici aplicatia isi stocheaza fisiere temporar. de exemplu daca 
lucrezi la un fisier si OS-ul pica, in momentul in care se va restarta, va cauta aici sa vada
daca a fost o copie salvata recent 
13. usr - "universal system resources" - sistem de fisiere secundar, aici aplicatiile folosite de
useri vor fi instalate. daca nu gasim un binar sub /bin putem cauta aici. spre exemplu: awk, less
scp, aici isi au stocate binarele.
14. var - date variabile (fisiere jurnal, cache) - mi se pare un folder important, dar l-am omis 
pe slide, aici gasim fisiere la care ne asteptam sa creasca in size, de exemplu /var/log, /var/crash.. 
15. proc - aici gasim de exemplu informatii despre procese sistemului. orice proces va avea un
director aici cu toate informatiile necesare. de asemenea aici gasim info despre CPU si Memory