CONDITIONAL:

Uneori este necesar sa specificam o directie a unei actiuni care trebuie intreprinsa mai departe in shell-ul nostru in functie de un rezultat, de un esec sau un success al unei comenzi. Conditia if ne permite sa punem conditii.

Inainte de asta sa definim 2 termeni foarte importanti: Conditie si Expresie.
O conditie in bash scripting e nimic altceva decat o comparatie intre 2 valori.
O expresie poate fi o combinatie de valori si operatii matematice.. - + / etc. EX: 5+2, a<3..

if TEST-COMMANDS(bash conditional expression); then COMMANDS; fi
# if [ 2 = 2 ]; then echo yeeees; fi #

Sa explic comanda de mai sus: DACA valoarea 2 este egala cu 2 inseamna ca exist statusul meu este 0, indica succes. Daca operatiunea noastra ar fi gresita, am avea o eroare si un alt exit status. Daca avem exit status 0, mergem mai departe cu THEN, daca nu, ne oprim aici.

Asadar zic sa impartim in 2 sintaxa asta in felul urmator: pana la then si dupa then.

In prima parte gasim if TEST-COMMANDS.. care este executata si, daca exit statusul este 0 (succesSs), se executa partea a 2-a. 

TEST:
Dar ce este TEST, ca tot am vorbit de TEST-COMMANDS
De departe, comanda utilizată cel mai frecvent cu if este TEST. Comanda TEST execută o varietate de verificări și comparații. Poate avea două forme
Prima: test expression
Și a doua formă, mai populară: [expresie] unde expresia este o expresie care este evaluată ca fiind adevărată sau falsă. Comanda de testare returnează o stare de ieșire de 0 când expresia este adevărată și o stare de 1 când expresia este falsa.
Este interesant de observat că atât test cât și [ sunt de fapt comenzi.
Comenzile test și [ acceptă o gamă largă de expresii și teste utile.

FILE EXPRESSIONS:
Inca o chestie interesanta, expresiile pot compara fisiere, se numesc file expressions. exemplu file1 -nt file2 inseamna ca file1 este newer than file2, sau -ot care e older than. sau # -f file # file exists as a regular file, o sa facem mai tarziu un exemplu.

Exemple de test commands(primary expressions) https://www.gnu.org/software/bash/manual/html_node/Bash-Conditional-Expressions.html

Revenind la expresii, o sa mai zic ca expresiile pot fi combinate:

[ ! EXPR ] -- Adevarat daca EXPR este falsa. # if [ ! 2 = 4 ]; then echo yeeees; else echo noooo; fi #

La fel cum if-ul este închis cu fi, paranteza pătrata de deschidere trebuie închisa după ce au fost enumerate condițiile.

In partea a 2-a, gasim un THEN.. acolo putem pune orice comanda de UNIX valida, orice program executabil, orice statement de shell, atata timp cat punem la sfarsi FI, sa inchidem, inversul lui IF. Foarte important de stiut este ca then si fi sunt considerate statementuri separate, de aceea avem nevoie dupa then sa punem ; 
 
Propun sa facem un exemplu mic, sa facem un script care cauta un fisier, si daca exista atunci sa ne dea un mesaj, daca nu, sa ne dea alt mesaj:

#
#!/bin/bash

echo "This scripts checks the existence of the messages file."
echo "Checking..."
sleep 5
if [ -f /var/log/messages ]
  then
    echo "/var/log/messages exists."
fi
echo
echo "...done."
#

else, elif --> optionale

# exemplu elif 
declare x=6

if [ "$x" -eq 4 ]; then
        echo 'x equal to 5'
elif [ "$x" -eq 5 ]; then
        echo 'asa da'
else
        echo 'nici asta nu e bun'
fi
#