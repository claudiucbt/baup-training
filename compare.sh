#!/bin/bash

function float_gt() {
    perl -e "{if($1>=$2){print 1} else {print 2}}"
}

lint_score=$(cat lint_score)
number=8

if [[ $(float_gt $lint_score $number) == 1 ]]; then
        echo "ok"
else
        echo "not good bro"
        exit 1
fi