#!/usr/bin/env python

"""This script creates a WebServer"""

import flask

APP = flask.Flask(__name__)
APP.config["DEBUG"] = True

@APP.route('/', methods=['GET'])
def home():
    return "<h1>Ai reusit. Good job mate!</h1>\n"

APP.run()
